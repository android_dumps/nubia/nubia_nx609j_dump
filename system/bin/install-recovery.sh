#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:20964652:99d2f2ae21e8ba9a83ab55eb28609f85d27823cf; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:14804264:5a715dcc8a898804ed97d92cd9441651d99bbf63 EMMC:/dev/block/bootdevice/by-name/recovery 99d2f2ae21e8ba9a83ab55eb28609f85d27823cf 20964652 5a715dcc8a898804ed97d92cd9441651d99bbf63:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
